# Desafio-pokeapi

_Deployment Guide_

First, to run the app using the command: 
**mvn spring-boot:run**

The API Rest should be available now at: http://localhost:5000 (this is the port needed for the api to be compatible with Elastick Beanstalk)

For deploying, build a compiled .jar version of the app using the maven package command: 
**mvn clean package spring-boot:repackage**

This will create a compiled .jar file at: 
**./target/pokeapi-0.0.1-SNAPSHOT.jar**

Now create an S3 bucket and upload the compiled .jar file:
**aws s3 mb s3://desafio-pokeapi**

**aws s3 cp target/pokeapi-0.0.1-SNAPSHOT.jar s3://project-name/desafio-pokeapi-web.jar**

Finally deploy using Elastic Beanstalk

Creating an Elastic Beanstalk environment:
aws elasticbeanstalk create-application --application-name pokeapi
aws elasticbeanstalk create-environment --application-name pokeapi --cname-prefix pokeapi --environment-name pokeapi-env --solution-stack-name "64bit Amazon Linux 2018.03 v2.10.7 running Java 8"

To deploy the new application version to Elastic Beanstalk, now simply run the commands:
aws elasticbeanstalk create-application-version --application-name pokeapi --version-label "0.0.1" --source-bundle S3Bucket="desafio-pokeapi",S3Key="desafio-pokeapi-web.jar"
aws elasticbeanstalk update-environment --environment-name pokeapi-env --version-label "0.0.1"

Now, the API Rest should be available at:
**_http://pokeapi-env.eba-hsrnc62d.us-east-1.elasticbeanstalk.com/_**

# Desafio-pokeapi

_Endpoints_

**http://pokeapi-env.eba-hsrnc62d.us-east-1.elasticbeanstalk.com/pokemon?page=(number)**

Returns paginated list of available pokemons. A list will contain up to 20 resources. 
You can use 'page' to move to the next page, e.g. ?page=5

**http://pokeapi-env.eba-hsrnc62d.us-east-1.elasticbeanstalk.com/pokemon/{id}**

Returns a specific pokemon by its id.
