package com.desafio.pokeapi.service;

import com.desafio.pokeapi.domain.EvolutionChain;
import com.desafio.pokeapi.domain.Pokemon;
import com.desafio.pokeapi.domain.PokemonQueryResult;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Collections;

@Service
public class PokeApiService  {

    private final Logger log = LoggerFactory.getLogger(PokeApiService.class);
    HttpHeaders headers = new HttpHeaders();

    public PokeApiService() {
        setUpHttpConnection();
    }

    private void setUpHttpConnection() {
        CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
    }

    @Cacheable("pokemons")
    public PokemonQueryResult getPokemons(String offset) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
            PokemonQueryResult apiResponse = restTemplate.exchange("https://pokeapi.co/api/v2/pokemon?limit=20&offset=" + offset, HttpMethod.GET, entity, PokemonQueryResult.class).getBody();

            ArrayList<Pokemon> pokemons = new ArrayList<>();
            if (apiResponse != null) {
                apiResponse.getResults().parallelStream().forEach(pokemon -> {
                    Pokemon pkmn = restTemplate.exchange(pokemon.getUrl(), HttpMethod.GET, entity, Pokemon.class).getBody();
                    pokemons.add(pkmn);
                });
                apiResponse.setResults(pokemons);
            }
            return apiResponse;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }

    @Cacheable("pokemon/id")
    public Pokemon getPokemon(Long pokemonId) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
            Pokemon pokemon = restTemplate.exchange("https://pokeapi.co/api/v2/pokemon/" + pokemonId, HttpMethod.GET, entity, Pokemon.class).getBody();

            if (pokemon == null) return null;

            EvolutionChain evolutionChain = restTemplate.exchange("https://pokeapi.co/api/v2/evolution-chain/" + pokemonId, HttpMethod.GET, entity, EvolutionChain.class).getBody();
            pokemon.setEvolutionChain(evolutionChain);
            return pokemon;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
    }
}
