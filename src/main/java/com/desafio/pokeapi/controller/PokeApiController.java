package com.desafio.pokeapi.controller;

import com.desafio.pokeapi.domain.Pokemon;
import com.desafio.pokeapi.domain.PokemonQueryResult;
import com.desafio.pokeapi.service.PokeApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class PokeApiController {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final PokeApiService pokeApiService;

    public PokeApiController(PokeApiService pokeApiService) {
        this.pokeApiService = pokeApiService;
    }

    @GetMapping("/pokemon")
    public ResponseEntity<PokemonQueryResult> getPokemons(@RequestParam(defaultValue = "0") String page) {
        String offset = page.equals("0") ? page : String.valueOf((Integer.parseInt(page) * 20));
        PokemonQueryResult pokemonQueryResult = pokeApiService.getPokemons(offset);
        return new ResponseEntity<>(pokemonQueryResult, HttpStatus.OK);
    }

    @GetMapping("/pokemon/{id}")
    public ResponseEntity<Pokemon> getPokemon(@PathVariable Long id) throws Exception {
        if (id == null) throw new Exception("Id can not be null");
        Pokemon pokemon = pokeApiService.getPokemon(id);
        return new ResponseEntity<>(pokemon, HttpStatus.OK);
    }
}
