package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonType {

  public PokemonType() {
  }

  public PokemonType(Type type) {
    this.type = type;
  }

  private Type type;

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }
}