package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChainLink {

  public ChainLink() {
  }

  public ChainLink(PokemonSpecies species, ArrayList<ChainLink> evolves_to) {
    this.species = species;
    this.evolves_to = evolves_to;
  }

  private PokemonSpecies species;

  private ArrayList<ChainLink> evolves_to;

  public PokemonSpecies getSpecies() {
    return species;
  }

  public void setSpecies(PokemonSpecies species) {
    this.species = species;
  }

  public ArrayList<ChainLink> getEvolves_to() {
    return evolves_to;
  }

  public void setEvolves_to(ArrayList<ChainLink> evolves_to) {
    this.evolves_to = evolves_to;
  }
}