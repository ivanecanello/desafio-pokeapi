package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pokemon {

    public Pokemon() {
    }

    public Pokemon(int id, int height, String name, String url, int weight, PokemonSprites sprites, ArrayList<PokemonAbility> abilities, ArrayList<PokemonForm> forms, ArrayList<PokemonType> types, EvolutionChain evolutionChain) {
        this.id = id;
        this.height = height;
        this.name = name;
        this.url = url;
        this.weight = weight;
        this.sprites = sprites;
        this.abilities = abilities;
        this.forms = forms;
        this.types = types;
        this.evolutionChain = evolutionChain;
    }

    private int id;

    private int height;

    private String name;

    private String url;

    private int weight;

    private PokemonSprites sprites;

    private ArrayList<PokemonAbility> abilities;

    private ArrayList<PokemonForm> forms;

    private ArrayList<PokemonType> types;

    private EvolutionChain evolutionChain;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public ArrayList<PokemonAbility> getAbilities() {
        return abilities;
    }

    public void setAbilities(ArrayList<PokemonAbility> abilities) {
        this.abilities = abilities;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<PokemonForm> getForms() {
        return forms;
    }

    public void setForms(ArrayList<PokemonForm> forms) {
        this.forms = forms;
    }

    public PokemonSprites getSprites() {
        return sprites;
    }

    public void setSprites(PokemonSprites sprites) {
        this.sprites = sprites;
    }

    public ArrayList<PokemonType> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<PokemonType> types) {
        this.types = types;
    }

    public EvolutionChain getEvolutionChain() {
        return evolutionChain;
    }

    public void setEvolutionChain(EvolutionChain evolutionChain) {
        this.evolutionChain = evolutionChain;
    }
}