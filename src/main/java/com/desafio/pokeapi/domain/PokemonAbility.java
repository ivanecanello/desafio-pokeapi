package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonAbility {

  public PokemonAbility() {
  }

  public PokemonAbility(int id, String url, Ability ability) {
    this.id = id;
    this.url = url;
    this.ability = ability;
  }

  private int id;

  private String url;

  private Ability ability;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Ability getAbility() {
    return ability;
  }

  public void setAbility(Ability ability) {
    this.ability = ability;
  }
}