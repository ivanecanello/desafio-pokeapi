package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonForm {

  public PokemonForm() {
  }

  public PokemonForm(int id, String name, int order, int form_order, boolean is_default, boolean is_battle_only, boolean is_mega, String form_name, Pokemon pokemon, PokemonFormSprites sprites) {
    this.id = id;
    this.name = name;
    this.order = order;
    this.form_order = form_order;
    this.is_default = is_default;
    this.is_battle_only = is_battle_only;
    this.is_mega = is_mega;
    this.form_name = form_name;
    this.pokemon = pokemon;
    this.sprites = sprites;
  }

  private int id;

  private String name;

  private int order;

  private int form_order;

  private boolean is_default;

  private boolean is_battle_only;

  private boolean is_mega;

  private String form_name;

  private Pokemon pokemon;

  private PokemonFormSprites sprites;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public int getForm_order() {
    return form_order;
  }

  public void setForm_order(int form_order) {
    this.form_order = form_order;
  }

  public boolean isIs_default() {
    return is_default;
  }

  public void setIs_default(boolean is_default) {
    this.is_default = is_default;
  }

  public boolean isIs_battle_only() {
    return is_battle_only;
  }

  public void setIs_battle_only(boolean is_battle_only) {
    this.is_battle_only = is_battle_only;
  }

  public boolean isIs_mega() {
    return is_mega;
  }

  public void setIs_mega(boolean is_mega) {
    this.is_mega = is_mega;
  }

  public String getForm_name() {
    return form_name;
  }

  public void setForm_name(String form_name) {
    this.form_name = form_name;
  }

  public Pokemon getPokemon() {
    return pokemon;
  }

  public void setPokemon(Pokemon pokemon) {
    this.pokemon = pokemon;
  }

  public PokemonFormSprites getSprites() {
    return sprites;
  }

  public void setSprites(PokemonFormSprites sprites) {
    this.sprites = sprites;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}