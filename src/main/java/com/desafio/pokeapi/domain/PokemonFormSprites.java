package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonFormSprites {

  public PokemonFormSprites() {
  }

  public PokemonFormSprites(String front_default, String front_shiny, String back_default, String back_shiny) {
    this.front_default = front_default;
    this.front_shiny = front_shiny;
    this.back_default = back_default;
    this.back_shiny = back_shiny;
  }

  // The default depiction of this Pokémon form from the front in battle.
  private String front_default;

  // The shiny depiction of this Pokémon form from the front in battle.
  private String front_shiny;

  // The default depiction of this Pokémon form from the back in battle.
  private String back_default;

  // The shiny depiction of this Pokémon form from the back in battle.
  private String back_shiny;

  public String getFront_default() {
    return front_default;
  }

  public void setFront_default(String front_default) {
    this.front_default = front_default;
  }

  public String getFront_shiny() {
    return front_shiny;
  }

  public void setFront_shiny(String front_shiny) {
    this.front_shiny = front_shiny;
  }

  public String getBack_default() {
    return back_default;
  }

  public void setBack_default(String back_default) {
    this.back_default = back_default;
  }

  public String getBack_shiny() {
    return back_shiny;
  }

  public void setBack_shiny(String back_shiny) {
    this.back_shiny = back_shiny;
  }
}