package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonQueryResult {

  public PokemonQueryResult(int count, String next, String previous, ArrayList<Pokemon> results) {
    this.count = count;
    this.next = next;
    this.previous = previous;
    this.results = results;
  }

  public PokemonQueryResult() {
  }

  private int count;

  private String next;

  private String previous;

  private ArrayList<Pokemon> results;

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }

  public String getNext() {
    return next;
  }

  public void setNext(String next) {
    this.next = next;
  }

  public String getPrevious() {
    return previous;
  }

  public void setPrevious(String previous) {
    this.previous = previous;
  }

  public ArrayList<Pokemon> getResults() {
    return results;
  }

  public void setResults(ArrayList<Pokemon> results) {
    this.results = results;
  }
}