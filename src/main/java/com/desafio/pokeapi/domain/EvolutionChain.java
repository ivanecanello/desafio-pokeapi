package com.desafio.pokeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EvolutionChain {

  public EvolutionChain() {
  }

  public EvolutionChain(int id, ChainLink chain) {
    this.id = id;
    this.chain = chain;
  }

  private int id;

  private ChainLink chain;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public ChainLink getChain() {
    return chain;
  }

  public void setChain(ChainLink chain) {
    this.chain = chain;
  }
}