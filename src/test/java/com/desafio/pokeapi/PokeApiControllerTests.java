package com.desafio.pokeapi;

import com.desafio.pokeapi.domain.Pokemon;
import com.desafio.pokeapi.domain.PokemonQueryResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PokeapiApplication.class)
@WebAppConfiguration
class PokeApiControllerTests {
	protected MockMvc mvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void getPokemons() throws Exception {
		String uri = "/pokemon";
		setUp();
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		PokemonQueryResult pokemons = mapFromJson(content, PokemonQueryResult.class);
		assertTrue(pokemons.getResults().size() > 0);
	}

	@Test
	public void getPokemon() throws Exception {
		String uri = "/pokemon";
		int id = 2;
		setUp();
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/" + id)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Pokemon pokemon = mapFromJson(content, Pokemon.class);
		assertNotNull(pokemon);
	}
}
