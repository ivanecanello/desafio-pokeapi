package com.desafio.pokeapi;

import com.desafio.pokeapi.controller.PokeApiController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PokeapiApplicationTests {

	@Autowired
	private PokeApiController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
